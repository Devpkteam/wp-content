<?php
	register_nav_menus( array(
		'Primary_Menu' => ' Navigation Menu',
		'footer_menu' => 'My Custom Footer Menu',
	) );
	
	add_action( 'widgets_init', 'theme_slug_widgets_init' );
	function theme_slug_widgets_init() {
	    register_sidebar( 
	    	array(
	        'name' => __( 'Footer Col 1', 'theme-slug' ),
	        'id' => 'footer-1',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	        'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
		    ));
	}
	add_action( 'widgets_init', 'theme_slug_widgets_init1' );
	function theme_slug_widgets_init1() {
	    register_sidebar( 
	    	array(
	        'name' => __( 'Footer Col 2', 'theme-slug' ),
	        'id' => 'footer-2',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	        'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
		    ));
	}
	add_action( 'widgets_init', 'theme_slug_widgets_init2' );
	function theme_slug_widgets_init2() {
	    register_sidebar( 
	    	array(
	        'name' => __( 'Footer Col 2', 'theme-slug' ),
	        'id' => 'footer-3',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	        'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
		    ));
	}
	add_action( 'widgets_init', 'theme_slug_widgets_init3' );
	function theme_slug_widgets_init3() {
	    register_sidebar( 
	    	array(
	        'name' => __( 'Footer Col 3', 'theme-slug' ),
	        'id' => 'footer-4',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	        'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
		    ));
	}