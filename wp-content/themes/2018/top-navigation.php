<nav id="mynav" class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img src="<?php echo bloginfo('template_url');?>/images/logo.png" alt="Logo" class="navbar-brand-img">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
   <?php
  if ( has_nav_menu( 'Primary_Menu' ) ) {
       wp_nav_menu( array( 'theme_location' => 'Primary_Menu' , 'container_class'=> 'collapse navbar-collapse', 'container_id' => 'navbarSupportedContent' , 'menu_class'=>'navbar-nav ml-auto custombar',  ) );
  } ?> 
</nav>    


