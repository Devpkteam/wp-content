<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            <?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
            	<ul class="sidebar">
            		<?php dynamic_sidebar( 'footer-1' ); ?>
            	</ul>
            <?php }  ?>
            </div>
            <div class="col-md-3">
            <?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
            	<ul class="sidebar">
            		<?php dynamic_sidebar( 'footer-2' ); ?>
            	</ul>
            <?php }  ?>
            </div>
            <div class="col-md-3">
             <?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
            	<ul class="sidebar">
            		<?php dynamic_sidebar( 'footer-3' ); ?>
            	</ul>
            <?php }  ?>
            </div>
            <div class="col-md-3">
                 <?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
            	<ul class="sidebar">
            		<?php dynamic_sidebar( 'footer-4' ); ?>
            	</ul>
            <?php }  ?>
            </div>
        </div>
    </div>
</footer>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet/less" type="text/css" href="<?php echo bloginfo('template_url');?>/less/base.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="<?php echo bloginfo('template_url');?>/js/velocity.min.js" ></script>
    <?php wp_footer(); ?>
    <script type="text/javascript">
    console.log('script')
        $(window).scroll(function (event) {
             var scroll = $(window).scrollTop();
            // Do something
            console.log(scroll)
            if(scroll > 150){
                $("#mynav").addClass("no-top");
            }else{
                $("#mynav").removeClass("no-top");
            }
        });
    
    // A $( document ).ready() block.
    $( document ).ready(function() {
        console.log( "ready!" );
        $(".preload").addClass("animateOut");
    });
    $('div.client').mouseenter(function(){
        console.log('addClass');
        $("#circle-1").addClass('loop-1'); 
        $("#circle-2").addClass('loop-2'); 
        $("#circle-3").addClass('loop-2'); 
        $("#circle-4").addClass('loop-1'); 
        $("#circle-5").addClass('loop-2'); 
        $("#circle-6").addClass('loop-1'); 
        $("#circle-7").addClass('loop-2'); 
        $("#circle-8").addClass('loop-1'); 
        $("#circle-9").addClass('loop-2'); 
        $("#circle-10").addClass('loop-1');
        $("#circle-11").addClass('loop-2'); 
        $("#circle-12").addClass('loop-2');
        $("#circle-13").addClass('loop-1');
        $("#circle-14").addClass('loop-1');
    });
    </script>
</body> 
</html>